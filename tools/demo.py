# import base64
# from Crypto.Cipher import AES
# from Crypto.Util.Padding import unpad
#
#
# def add_to_32(text):
#     while len(text) % 16 != 0:
#         text += "\0"
#     return text
#
#
# def decrypt(key, msg):
#     key = add_to_32(key)
#     decode = base64.b64decode(msg)
#     aeskey = base64.b64decode(key)
#     Cryptor = AES.new(aeskey, AES.MODE_ECB)
#     plain_text = Cryptor.decrypt(decode)
#     bin_decrypt_result = unpad(plain_text, AES.block_size)  # 输出的是二进制Unicode编码
#     b = bin_decrypt_result.decode('utf8')
#     return b
#
#
# with open("./data.txt", encoding="utf-8") as data:
#     lines = data.readlines()
#     lines_str = ''
#     for line in lines:
#         lines_str += line.strip()
#
# datas = lines_str
# aeskey = "kJrOctnrtdj0obkMkdDMfVptvJYEi9BgiZP/m5T5n84="
#
# if __name__ == '__main__':
#     print("原始数据--->" + datas)
#     data = decrypt(aeskey, datas)
#     print("解密--->" + data)

import requests
import json
from tools.hooks import decrypt, headers

url = "http://soloop-test.wanyol.com/common/setting/getLehuaCategory"
aeskey = "kJrOctnrtdj0obkMkdDMfVptvJYEi9BgiZP/m5T5n84="
r = requests.get(url=url, headers=headers())
result = r.text
print("数据--->" + result)
s = r.json()
s = json.loads(result)
datas = s["data"]
data = decrypt(aeskey, datas)
print("解密--->" + data)
